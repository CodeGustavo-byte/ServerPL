package dev.codegustavo.ServerPL.menus;

import dev.codegustavo.ServerPL.utils.GUIUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import static dev.codegustavo.ServerPL.utils.GUIUtils.*;

public class PlayerMngr {

    private static Inventory playerMngrGUI;


    public static void openPlayerMngrGUI(Player p, Player target) {
        Inventory playerMngrGUI = Bukkit.createInventory(p, 45, target.getName());

        playerMngrGUI.setItem(15, makeItem(Material.COMMAND_BLOCK, "§dGameMode", "§7Allows you to change the players gamemode"));
        playerMngrGUI.setItem(16, makeItem(Material.ENDER_CHEST, "§5Ender-Chest", "§7Allows you to see the player's ender chest"));
        playerMngrGUI.setItem(17, makeItem(Material.CHEST, "§eInventory", "§7Allows you to see the player's inventory"));

        playerMngrGUI.setItem(21, makePlayerSkull(target));

        playerMngrGUI.setItem(24, makeItem(Material.STICKY_PISTON, "§6Warpy-Piston", "§7Allows you to teleport the player to you"));
        //Finish Troll Menu
        playerMngrGUI.setItem(25, makeItem(Material.DEAD_BUSH, "§c(IN_PRODUCTION) Troll Menu", "§4In production"));
        playerMngrGUI.setItem(26, makeItem(Material.ENDER_PEARL, "§6Warpy-Pearl", "§7Allows you to teleport to the player's location"));

        playerMngrGUI.setItem(33, makeItem(Material.TOTEM_OF_UNDYING, "§aUnBan", "§7Allows you to unban the player if banned"));
        playerMngrGUI.setItem(34, makeItem(Material.BARRIER, "§4Kick", "§7Allows you to kick the player"));
        playerMngrGUI.setItem(35, makeItem(Material.IRON_BARS, "§4WARNING-Ban's the player", "§7READ THE NAME"));
        fillGaps(45, playerMngrGUI);


        p.openInventory(playerMngrGUI);
    }




}
