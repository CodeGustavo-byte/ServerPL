package dev.codegustavo.ServerPL.menus;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import static dev.codegustavo.ServerPL.utils.GUIUtils.*;

public class PlayerList {

    private static Inventory playerListGUI;

    public static void openPlayerListGUI(Player p) {
        playerListGUI = Bukkit.createInventory(p, 27, "Player List");

        for (Player player : Bukkit.getOnlinePlayers()) {
            playerListGUI.addItem(makePlayerSkull(player));
        }


    }


}
