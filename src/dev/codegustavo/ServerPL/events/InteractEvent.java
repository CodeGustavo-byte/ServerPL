package dev.codegustavo.ServerPL.events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class InteractEvent implements Listener {
    @EventHandler
    public void interactEvent(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (player.getInventory().getItemInMainHand().getType() == Material.ENDER_CHEST) {
            if (player.isSneaking()) {
                if (e.getAction() == Action.RIGHT_CLICK_AIR) {
                    player.openInventory(player.getEnderChest());
                } else if (e.getAction() == Action.LEFT_CLICK_AIR) {
                    if (player.isOp()) {
                        player.openInventory(player.getEnderChest());
                    }
                }
            }

        }


    }
}