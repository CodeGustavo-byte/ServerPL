package dev.codegustavo.ServerPL;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SaveToFile {

    public static void saveWarps() {

        if (Main.plugin.getConfig().contains("data")) {
            Main.plugin.getConfig().set("data","");
        }

        for (Map.Entry<String, Location> entry : Main.WARP_LOC_GET.entrySet()) {
            Main.plugin.getConfig().set("data." + entry.getKey(), entry.getValue());
        }
        Main.plugin.saveConfig();

    }


    public static void restoreWarps() {
        Main.plugin.getConfig().getConfigurationSection("data").getKeys(false).forEach(key ->{
            Location content = ((Location) Main.plugin.getConfig().get("data."+key));
            Main.WARP_LOC_GET.put(key, content);
        });

    }
}
