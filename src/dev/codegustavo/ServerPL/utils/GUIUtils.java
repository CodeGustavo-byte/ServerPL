package dev.codegustavo.ServerPL.utils;

import dev.codegustavo.ServerPL.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GUIUtils {

    private static ItemStack GAP_GLASS = makeItem(Material.LIGHT_GRAY_STAINED_GLASS_PANE, " ", " ");

    public static ItemStack makeItem(Material mat, String displayName, String... lore) {
        ItemStack item = new ItemStack(mat);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack makePlayerSkull(Player p) {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta meta = (SkullMeta) item.getItemMeta();
        meta.setOwningPlayer(Bukkit.getOfflinePlayer(p.getUniqueId()));
        List<String> lore = new ArrayList<>();
        lore.add("§6"+p.getName());
        lore.add("§6Health: "+p.getHealth());
        lore.add("§6XP: "+p.getExp());
        item.setItemMeta(meta);
        return item;
    }

    public static void fillGaps(int invSize, Inventory inv) {
        for (int i = 0; i < invSize; i++) {
            if (inv.getItem(i) == null) {
                inv.setItem(i, GAP_GLASS);
            }
        }
    }

}
