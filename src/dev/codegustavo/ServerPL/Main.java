package dev.codegustavo.ServerPL;

import dev.codegustavo.ServerPL.commands.*;
import dev.codegustavo.ServerPL.events.InteractEvent;
import dev.codegustavo.ServerPL.events.InvClickEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public final class Main extends JavaPlugin {

    public static Main plugin;
    public static Map<String, Location> WARP_LOC_GET = new HashMap<>();
    public static Map<Player, Player> MP = new HashMap<>();

    @Override
    public void onEnable() {
        plugin = this;
        this.getCommand("warp").setExecutor(new Warp());
        this.getCommand("set").setExecutor(new Set());

        this.getCommand("warp").setTabCompleter(new WarpComp());
        this.getCommand("set").setTabCompleter(new SetComp());

        this.getServer().getPluginManager().registerEvents(new InteractEvent(), this);
        this.getServer().getPluginManager().registerEvents(new InvClickEvent(), this);


        this.saveDefaultConfig();
        if (this.getConfig().contains("data")) {
            SaveToFile.restoreWarps();
        }
    }

    @Override
    public void onDisable() {
        if (!WARP_LOC_GET.isEmpty()) {
            SaveToFile.saveWarps();
        }
    }

}
