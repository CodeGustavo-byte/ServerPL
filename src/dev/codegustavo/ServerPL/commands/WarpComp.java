package dev.codegustavo.ServerPL.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import dev.codegustavo.ServerPL.Main;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WarpComp implements TabCompleter {


    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {


        if (strings.length == 1) {
            List<String> result = new ArrayList<String>();
            for (Map.Entry<String, Location> entry : Main.WARP_LOC_GET.entrySet()) {
                if (entry.getKey().contains(commandSender.getName())) {
                    String thingy = entry.getKey().toLowerCase();
                    thingy = thingy.replace("_" + commandSender.getName(), "");
                    result.add(thingy);
                }
            }
            if (commandSender.isOp()) {
                result.add("player");
            }
            return result;
        } else if (strings.length == 2) {
            List<String> players = new ArrayList<String>();
            for (Player p : Bukkit.getOnlinePlayers()) {
                players.add(p.getName());
            }
            return players;
        }

        return null;
    }
}
