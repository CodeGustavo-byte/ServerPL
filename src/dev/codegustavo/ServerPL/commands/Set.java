package dev.codegustavo.ServerPL.commands;

import dev.codegustavo.ServerPL.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

public class Set implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("set")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You're not a player");
                return true;
            }

            if (!(args.length >= 2)) {return true;}
            Player player = (Player) sender;
            String WARP_NAME = args[1];

            if (args[0].equals("add")) {
                if (!(Main.WARP_LOC_GET.containsKey(WARP_NAME+"_"+player.getName().toLowerCase()))) {
                    Main.WARP_LOC_GET.put(WARP_NAME+"_"+player.getName().toLowerCase(), player.getLocation());
                } else {
                    Main.WARP_LOC_GET.replace(WARP_NAME+"_"+player.getName().toLowerCase(), player.getLocation());
                }
                player.sendMessage("§aYour warp \""+WARP_NAME+"\" was set!\n §6You can warp to it by typing \"/warp "+WARP_NAME+"\"");
            } else if (args[0].equals("remove")) {
                if (Main.WARP_LOC_GET.containsKey(WARP_NAME+"_"+player.getName().toLowerCase())) {
                    Main.WARP_LOC_GET.remove(WARP_NAME+"_"+player.getName().toLowerCase());
                } else {
                    player.sendMessage("§cThat warp does not exists");
                }
            }




        }
        return true;
    }
}