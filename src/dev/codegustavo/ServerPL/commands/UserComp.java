package dev.codegustavo.ServerPL.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class UserComp implements TabCompleter {
    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {

        if (strings.length == 1){
            List<String> result = new ArrayList<>();
            for (Player p : Bukkit.getOnlinePlayers()) {
                result.add(p.getName());
            }
            return result;
        } else if (strings.length == 2){
            List<String> result = new ArrayList<>();
            result.add("kick");
            result.add("echest");
            result.add("chest");
            result.add("epearl");
            result.add("spiston");
        }


        return null;
    }
}
