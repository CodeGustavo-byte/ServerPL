package dev.codegustavo.ServerPL.commands;

import dev.codegustavo.ServerPL.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

public class Warp implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("warp")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.BLACK + "You're not a player");
                return true;
            }

            if (!(args.length >= 1)) {return true;}
            Player player = (Player) sender;
            String WARP_HOME = args[0];

            if (WARP_HOME.equals("player")) {
                Player victim = (Player) Bukkit.getPlayerExact(args[1]);
                player.teleport(victim.getLocation());
            }


            if (Main.WARP_LOC_GET.containsKey(WARP_HOME+"_"+player.getName().toLowerCase())) {
                Location WARP_TO = Main.WARP_LOC_GET.get(WARP_HOME+"_"+player.getName().toLowerCase());
                player.teleport(WARP_TO);
                player.sendMessage("§aWarp sucessful");
            }

        }
        return true;
    }
}