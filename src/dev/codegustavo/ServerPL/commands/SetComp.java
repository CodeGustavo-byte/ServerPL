package dev.codegustavo.ServerPL.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import dev.codegustavo.ServerPL.Main;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SetComp implements TabCompleter{

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {

        if (strings.length == 1) {
            List<String> result = new ArrayList<String>();
            result.add("add"); result.add("remove");
            return result;
        } else if (strings.length == 2 && strings[0].equals("remove")) {
            List<String> warps = new ArrayList<String>();
            for (Map.Entry<String, Location> entry : Main.WARP_LOC_GET.entrySet()) {
                if (entry.getKey().contains(commandSender.getName())) {
                    String thingy = entry.getKey().toLowerCase();
                    thingy = thingy.replace("_" + commandSender.getName(), "");
                    warps.add(thingy);
                }
            }
            return warps;
        }

        return null;
    }
}